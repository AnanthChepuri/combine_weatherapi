//
//  ViewController.swift
//  Combine_WeatherAPI
//
//  Created by Ananth Chepuri on 17/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import UIKit
import Combine
import CoreLocation

class ViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var temparatureTextField: UITextField!
    @IBOutlet weak var diceImageView1: UIImageView!
    @IBOutlet weak var diceImageView2: UIImageView!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    let locManager = CLLocationManager()
    @IBOutlet weak var viewLocationInfo: UIView!

    var webService = WebService()
    var cancellable: AnyCancellable?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewLocationInfo.isHidden = true
        locManager.requestWhenInUseAuthorization()
        locManager.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.temparatureTextField.resignFirstResponder()
        getWeatherDataForCity()
        return true
    }

    func getWeatherDataForCity() {
        self.cancellable = self.webService.fetchWeather(long: longitudeLabel.text!, lat: latitudeLabel.text!)
            .catch { _ in Just(Weather.placeHiolder) }
            .map { "Current Temp: \($0.temp ?? 0.0 ) ℃\n Min Temp: \($0.temp_min ?? 0.0) ℃\n Max Temp: \($0.temp_max ?? 0.0)" }
            .assign(to: \.text, on: self.temperatureLabel)
    }
    
    @IBAction func getTemperatureClicked(_ sender: UIButton) {
        viewLocationInfo.isHidden = false
        getCurrentLocationLongitudeAndLatitude()
        getWeatherDataForCity()
    }
    
    func getCurrentLocationLongitudeAndLatitude() {
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return
            }
            
            let long: Double = currentLocation.coordinate.longitude
            let lat: Double = currentLocation.coordinate.latitude
            
            longitudeLabel.text = String(format: "%.6f", long)    //"Longitude = \(currentLocation.coordinate.longitude)"
            latitudeLabel.text = String(format: "%.6f", lat)     //"Latitude = \(currentLocation.coordinate.latitude)"
        }
    }
    
    @IBAction func rollButtonPressed(_ sender: UIButton) {
        let diceArray = [#imageLiteral(resourceName: "DiceOne"), #imageLiteral(resourceName: "DiceTwo"), #imageLiteral(resourceName: "DiceThree"), #imageLiteral(resourceName: "DiceFour"), #imageLiteral(resourceName: "DiceFive"), #imageLiteral(resourceName: "DiceSix")]
        diceImageView1.image = diceArray[Int.random(in: 0...5)]
        diceImageView2.image = diceArray[Int.random(in: 0...5)]
    }
}

