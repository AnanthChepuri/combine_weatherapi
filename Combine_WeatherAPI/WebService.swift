//
//  WebService.swift
//  Combine_NetworkCall
//
//  Created by Ananth Chepuri on 17/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation
import Combine

class WebService {
    
    func fetchWeather (long: String, lat: String) -> AnyPublisher<Weather, Error> {
        
        let url = "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(long)&appid=8c1251e74c464da5920fb4e41a817a0b&units=metric"
        
        print(url)
        guard let weatherURL = URL(string: url) else {
            fatalError("Invalid url")
        }
        
        return URLSession.shared.dataTaskPublisher(for: weatherURL)
            .map { $0.data }
            .decode(type: WeatherResponse.self, decoder: JSONDecoder())
            .map { $0.main }
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}
