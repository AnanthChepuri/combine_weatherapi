//
//  Weather.swift
//  Combine_NetworkCall
//
//  Created by Ananth Chepuri on 17/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

struct WeatherResponse: Decodable {
    var main: Weather
    var sys: Sys
}

struct Weather: Decodable {
    var temp: Double?
    var temp_min: Double?
    var temp_max: Double?

    static var placeHiolder: Weather {
        return Weather(temp: nil, temp_min: nil, temp_max: nil)
    }
}

struct Sys: Decodable {
    let country: String

    static var placeHiolder: Sys {
        return Sys(country: "")
    }
}
