//
//  Combine_WeatherAPIUITests.swift
//  Combine_WeatherAPIUITests
//
//  Created by Ananth Chepuri on 18/05/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import XCTest

class Combine_WeatherAPIUITests: XCTestCase {
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        let app = XCUIApplication()
        app.launch()

//        let weatherLabel = app.staticTexts["Weather"]
//        let enterCityLabel = app.staticTexts["Enter City"]
//        let cityTextField = app.textFields["Temparature Text Field"]
//        let getTemperatureBtn = app.buttons["Get Temperature"]
//        let temperatureLabel = app.staticTexts["Temperature Label"]
//
//        XCTAssertEqual(weatherLabel.exists, true)
//        XCTAssertEqual(enterCityLabel.exists, true)
//        XCTAssertEqual(cityTextField.exists, true)
//        XCTAssertEqual(getTemperatureBtn.exists, true)
//        XCTAssertEqual(temperatureLabel.exists, true)
//
//        XCTAssertEqual("Weather", weatherLabel.value as! String)
//        XCTAssertEqual("Enter City:", enterCityLabel.value as! String)
//        XCTAssertEqual("Temperature in ℃", temperatureLabel.value as! String)
//
//        cityTextField.tap()
//        cityTextField.typeText("London")
//        print(cityTextField)
//
//        getTemperatureBtn.tap()
//        print(cityTextField)
    }

//    func testLaunchPerformance() throws {
//        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
//            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
//                XCUIApplication().launch()
//            }
//        }
//    }
}
